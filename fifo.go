package mountisa

import "sync"

type FiFo[T any] struct {
	mx   sync.Mutex
	vals []T
}

func (f *FiFo[T]) Add(new T) {
	f.mx.Lock()
	defer f.mx.Unlock()
	f.vals = append(f.vals, new)
}

func (f *FiFo[T]) Next() (next T, ok bool) {
	f.mx.Lock()
	defer f.mx.Unlock()
	if len(f.vals) != 0 {
		next = f.vals[0]
		ok = true
		if len(f.vals) == 1 {
			f.vals = nil
		} else {
			f.vals = f.vals[1:]
		}
	}
	return
}
